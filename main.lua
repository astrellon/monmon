require("camera")
require("strict")
require("textured_polygon")

local screenWidth = 0
local screenHeight = 0
local player = nil
local box = nil
local stuff = nil
local image = nil

function love.resize(width, height)
    camera:setBounds(0, 0, width, height)
    screenWidth = width
    screenHeight = height
end

function love.load()
    love.window.setMode(800, 600, {resizable = true, vsync = false, minwidth = 400, minheight = 300})
    screenWidth = love.graphics.getWidth()
    screenHeight = love.graphics.getHeight()
    camera:setBounds(0, 0, screenWidth, screenHeight)
    camera:setScale(0.5, 0.5)

    local texture=love.image.newImageData('assets/texture.png')
    local bkg=love.graphics.newImage(texture)
    local polygon={100,100, 200,100, 300,200, 400,100, 520,100, 530,200, 400,200, 300,300, 200,200, 100,200}

    image=TexturedPolygon(polygon,texture)

    player = {
        x = screenWidth / 2,
        y = screenHeight / 2,
        width = 50,
        height = 50,
        speed = 300,
        color = {0, 150, 0}
    }

    box = {
        x = 5,
        y = 5,
        width = screenWidth * 2 - 10,
        height = screenHeight * 2 - 10,
        color = {0, 0, 1}
    }

    stuff = {}

    for i = 1, 1000 do
        table.insert(
            stuff,
            {
                x = math.random(100, screenWidth * 2 - 100),
                y = math.random(100, screenHeight * 2 - 100),
                width = math.random(100, 300),
                height = math.random(100, 300),
                color = {math.random(), math.random(), math.random()}
            }
        )
    end
end

function love.update(dt)
    if love.keyboard.isDown("left") then
        player.x = player.x - player.speed * dt
    end
    if love.keyboard.isDown("right") then
        player.x = player.x + player.speed * dt
    end
    if love.keyboard.isDown("up") then
        player.y = player.y - player.speed * dt
    end
    if love.keyboard.isDown("down") then
        player.y = player.y + player.speed * dt
    end

    local scaleX = 2 / camera.scaleX
    local scaleY = 2 / camera.scaleY
    local posX = player.x + player.width * 0.5
    local posY = player.y + player.height * 0.5
    camera:setPosition(posX - screenWidth / scaleX, posY - screenHeight / scaleY)
end

function love.draw()
    camera:set()

    local g = love.graphics;

    -- box
    g.setColor(box.color)
    g.rectangle("line", box.x, box.y, box.width, box.height)

    -- stuff
    --[[
    for _, v in pairs(stuff) do
        g.setColor(v.color)
        g.rectangle("fill", v.x, v.y, v.width, v.height)
    end
    ]]--
    g.setColor(1,1,1)

    g.draw(image.img)

    -- player
    g.setColor(player.color)
    g.rectangle("fill", player.x, player.y, player.width, player.height)

    camera:unset()

    g.setColor(1,1,1)
    g.print("Current FPS: "..tostring(love.timer.getFPS( )), 10, 10)
end

function math.clamp(x, min, max)
    return x < min and min or (x > max and max or x)
end
